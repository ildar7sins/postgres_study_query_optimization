SET search_path TO postgres_air;
CREATE INDEX IF NOT EXISTS flight_departure_airport ON flight (departure_airport);
CREATE INDEX IF NOT EXISTS flight_scheduled_departure  ON flight (scheduled_departure);
CREATE INDEX IF NOT EXISTS flight_update_ts ON flight (update_ts);
CREATE INDEX IF NOT EXISTS booking_leg_booking_id ON booking_leg (booking_id);
CREATE INDEX IF NOT EXISTS booking_leg_update_ts ON booking_leg (update_ts);
CREATE INDEX IF NOT EXISTS account_last_name ON account (last_name);
